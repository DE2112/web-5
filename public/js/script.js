let modal = document.getElementById("modal");
let fName = document.getElementById("fName");
let lName = document.getElementById("lName");
function calculate() {
 let cost = document.getElementById("cost").value;
 let count = document.getElementById("count").value;
 let price = parseInt(cost) * parseInt(count);
 let fullName = fName.value + ' ' + lName.value;
 let result = document.getElementById("result");
 if (price <= 0 || Object.is(price, NaN)) {
  result.innerHTML = '<h3 class="alert alert-danger my-3">Введенные данные некорректны</h3>';
 } else {
  result.innerHTML = '<h3 class="alert alert-success my-3">' + fullName + ', стоимость вашего заказа - ' + price + ' рублей</h3>';
 }
 modal.style.display = "none";
 return false;
}
function closeModal() {
 let alertWindow = document.getElementsByClassName("alert")[0];
 if (fName.value != "" && lName.value != "") {
  modal.style.display = "none";
 } else if (typeof alertWindow === 'undefined') {
  lName.classList.add("my-3");
  let alertWindow = document.createElement("div");
  alertWindow.classList.add("alert");
  alertWindow.classList.add("alert-danger");
  alertWindow.innerHTML = "Введите все поля!";
  let modalBody = document.getElementById("modal-body");
  modalBody.appendChild(alertWindow);
 }
 return false;
}